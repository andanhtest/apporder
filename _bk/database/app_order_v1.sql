-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2019 at 05:57 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_order_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_expiry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `token_expiry`, `picture`, `mobile`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'van a', 'Dsy@gmail.com', NULL, '$2y$10$xhWV00VsUhiJVrAYFjoTVO8zt09a/xV52OzhWl9dOMRx/eIIdNoNi', 'Ha2TLwK3kjgeeuZUZUxyMvg6eLydbzGWua0yr76Ap3zScO1ZtZnSCve7BDmo', '1548867426', NULL, NULL, NULL, 1, NULL, '2019-01-30 16:56:46'),
(2, 'van b', 'YSq@gmail.com', NULL, '$2y$10$/tKP9G9KdcHSoTTdD6M8L.PaJoovldEQH/wgXsN6VafVMy3SkFouW', 'NNmEKB159qCxXy6f7Rp1aIU4fL4JZqx14L5p9odmNJe3BcrQa9kAxerwDSby', '1548484839', NULL, NULL, NULL, 1, NULL, '2019-01-26 06:40:19'),
(3, 'van c', '50B@gmail.com', NULL, '$2y$10$H5tbLjzI4PLCJpL99hSBw.fKLz8CdW6I.c5Pivpl.OkDaxJPSqw3i', 'IzatSZscnAaRZX9ElTRfslulQOcBlhVaw5SGGXYRETVKKagqm7ypjq8Hg9sr', NULL, NULL, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `type` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `link`, `summary`, `content`, `file`, `target`, `status`, `type`, `order`, `created_at`, `updated_at`) VALUES
(1, 'banner', NULL, 'mo tả', '<p>nội dung</p>', 'vDG5x2c0_636198104751087597_636186286968225178_amsung-S7-Edge-Blue-Coral-4.jpg', NULL, 1, 2, 0, '2019-01-28 15:47:38', '2019-01-28 15:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `banner_group`
--

DROP TABLE IF EXISTS `banner_group`;
CREATE TABLE `banner_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_group`
--

INSERT INTO `banner_group` (`id`, `name`, `alias`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Banner', 'banner', 1, '2019-01-27 17:00:00', '2018-11-26 01:07:38'),
(2, 'Banner Left', 'banner-left', 1, '2019-01-27 17:00:00', '2019-01-27 17:00:00'),
(3, 'Banner Right', 'banner-right', 1, '2019-01-27 17:00:00', '2019-01-27 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

DROP TABLE IF EXISTS `categorys`;
CREATE TABLE `categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `name`, `alias`, `summary`, `content`, `images`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Mỹ phẩm 1', 'my-pham', 'mô tả 1', '<p>nội dung 1</p>', '9p5mpjg7_8825_thumb_G_1442029802458.jpg', 1, 1, '2019-01-27 15:11:10', '2019-01-27 15:15:04'),
(2, 'Điện thoại', 'dien-thoai', 'mô tả', '<p>Nội dung</p>', 'nw88ALaq_11872_thumb_G_1458151049932.jpg', 1, 1, '2019-01-27 15:15:37', '2019-01-27 15:15:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_01_17_224815_my_migrate', 2),
(8, '2019_01_17_225332_table_my_migrate', 3),
(11, '2019_01_18_232731_create_users2s_table', 4),
(12, '2019_01_25_223839_table_admins', 4),
(14, '2019_01_26_161124_table_news_group', 5),
(15, '2019_01_27_131603_table_news', 6),
(16, '2019_01_27_134012_add_news_group_id_to_news', 7),
(17, '2019_01_27_214743_table_products', 8),
(18, '2019_01_27_215101_table_categorys', 8),
(20, '2019_01_28_221735_table_banners', 9),
(21, '2019_01_28_222940_table_banner_group', 10),
(22, '2019_01_28_225107_table_orders', 11),
(23, '2019_01_28_225139_table_order_detail', 11),
(24, '2019_01_28_235724_add_phone_to_users', 12),
(25, '2019_01_28_235750_add_address_to_users', 12);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hot` int(11) NOT NULL DEFAULT '0',
  `new` int(11) NOT NULL DEFAULT '0',
  `special` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `news_group_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `alias`, `summary`, `content`, `images`, `hot`, `new`, `special`, `status`, `order`, `created_at`, `updated_at`, `news_group_id`) VALUES
(1, 'Tin tức 1.1.1', 'tin-tuc-1', 'mô tả1', '<p>nội dung1</p>', 'mUgTr1tq_636198104751087597_636186286968225178_amsung-S7-Edge-Blue-Coral-4.jpg', 0, 0, 0, 1, 1, '2019-01-27 06:25:36', '2019-01-27 06:57:19', 1),
(2, 'Tin tức 2', 'tin-tuc-2', 'Mô tả', '<p>Nội dung</p>', 'qPAy0oSP_11872_thumb_G_1458151049932.jpg', 0, 0, 0, 1, 1, '2019-01-27 06:28:08', '2019-01-27 06:28:08', 2),
(3, 'test chọn loại tin', 'test-chon-loai-tin', 'mô tả', '<p>nội dung</p>', 'Tftc1CMA_636198104751087597_636186286968225178_amsung-S7-Edge-Blue-Coral-4.jpg', 0, 0, 0, 1, 1, '2019-01-27 06:52:44', '2019-01-27 06:56:36', 3);

-- --------------------------------------------------------

--
-- Table structure for table `news_group`
--

DROP TABLE IF EXISTS `news_group`;
CREATE TABLE `news_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_group`
--

INSERT INTO `news_group` (`id`, `name`, `alias`, `summary`, `content`, `images`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Tin tức 1.1', 'tin-tuc', 'mô tả1', '<p>nội dung1</p>', 'WFkWTE6H_samsung-galaxy-s7-edge-black-1.png', 1, 1, '2019-01-26 16:31:39', '2019-01-27 06:31:56'),
(2, 'Giới thiệu', 'gioi-thieu', 'mô tả', '<p>nội dung</p>', '3LE0TDeT_636198104751087597_636186286968225178_amsung-S7-Edge-Blue-Coral-2.jpg', 0, 1, '2019-01-26 16:32:43', '2019-01-28 17:10:34'),
(3, 'Liên hệ', 'lien-he', 'Mô tả', '<p><img alt=\"\" src=\"/admin_asset/bower_components/kcfinder/upload/images/samsung-galaxy-s7-edge-black-1.png\" style=\"height:461px; width:400px\" />Nội<img alt=\"\" src=\"/uploads/data/images/8825_thumb_G_1442029802458.jpg\" style=\"height:261px; width:303px\" /><img alt=\"\" src=\"/admin_asset/bower_components/kcfinder/upload/images/ca-bay-mau.jpg\" style=\"height:251px; width:400px\" /> dung</p>', 'Y8DEamde_636198104751087597_636186286968225178_amsung-S7-Edge-Blue-Coral-4.jpg', 0, 1, '2019-01-27 05:25:43', '2019-01-27 05:58:16');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `fullname_buyer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_buyer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_buyer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_buyer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname_receiver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_receiver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_receiver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_receiver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` longtext COLLATE utf8mb4_unicode_ci,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `total` double NOT NULL DEFAULT '0',
  `vat` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `type` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `code`, `summary`, `user_id`, `fullname_buyer`, `phone_buyer`, `email_buyer`, `address_buyer`, `fullname_receiver`, `phone_receiver`, `email_receiver`, `address_receiver`, `location`, `note`, `total`, `vat`, `status`, `type`, `created_at`, `updated_at`) VALUES
(1, 'DH san pham', '001', 'abc', 0, 'nguyen van a', '0909090901', 'van@gmail.com', 'hcm', 'nguyen van a', '0909090901', 'van@gmail.com', 'hcm', 'hcm', 'abc', 100, 0, 0, 1, '2019-01-27 17:00:00', '2019-01-28 16:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `product_name`, `amount`, `price`, `created_at`, `updated_at`) VALUES
(1, '1', 1, 'san pham 1', 1, 100000, '2019-01-27 17:00:00', '2019-01-27 17:00:00'),
(2, '1', 2, 'san pham 2', 2, 200000, '2019-01-27 17:00:00', '2019-01-27 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `price_old` int(11) NOT NULL DEFAULT '0',
  `hot` int(11) NOT NULL DEFAULT '0',
  `new` int(11) NOT NULL DEFAULT '0',
  `view` int(11) NOT NULL DEFAULT '0',
  `special` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `alias`, `summary`, `content`, `images`, `price`, `price_old`, `hot`, `new`, `view`, `special`, `category_id`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Sản phẩm 1', 'san-pham-1', 'mô tả', '<p>nội dung</p>', 'qlIRrOWi_8825_thumb_G_1442029802458.jpg', 0, 0, 0, 0, 0, 0, 1, 0, 0, '2019-01-27 15:22:52', '2019-01-27 16:53:24'),
(2, 'Sản phẩm hot1', 'san-pham-hot', 'mô tả', '<p>nội dung</p>', 'Io89l1IA_8825_thumb_G_1442029802458.jpg', 0, 0, 0, 0, 0, 0, 2, 1, 0, '2019-01-27 16:37:45', '2019-01-27 16:51:20'),
(3, 'sp mỹ phẩm', 'sp-my-pham', 'mo ta', '<p>noi dung</p>', '', 0, 0, 0, 0, 0, 0, 1, 1, 0, '2019-01-27 16:49:42', '2019-01-27 16:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `test_table`
--

DROP TABLE IF EXISTS `test_table`;
CREATE TABLE `test_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `ten` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `address`) VALUES
(2, 'Nguyen van bb', 'Dsy@gmail.com', NULL, '$2y$10$XiN39IItWj4OogRrURo5O.LhRCjXbxP5tZNSx37L85rP/ZoxjKPNa', NULL, '2019-01-17 16:56:13', '2019-01-28 17:11:01', '0900909091', 'hcm 1'),
(4, 'van d', 'VD1@gmail.com', NULL, '$2y$10$CY5iJXNcMz/2UUpS86k.FOI6X7XQrPtuAnu4uNZb1Rhin1h9HnZFW', NULL, '2019-01-17 16:56:13', '2019-01-28 17:11:29', '0900909091', 'ha noi'),
(5, 'abced', 'abcde@gmail.com', NULL, '123456', NULL, '2019-01-17 16:56:13', '2019-01-17 16:56:13', '0900909091', 'hcm'),
(6, 'xKu', 'pmQ@gmail.com', NULL, '$2y$10$7ak9LYJ6W5JjDh0j58g9MOX8mEvoUkNV80UN4OyZRwL.53Zui9Q9i', NULL, '2019-01-17 16:57:05', '2019-01-17 16:57:05', '0900909091', 'hcm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_group`
--
ALTER TABLE `banner_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_group`
--
ALTER TABLE `news_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_table`
--
ALTER TABLE `test_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner_group`
--
ALTER TABLE `banner_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_group`
--
ALTER TABLE `news_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test_table`
--
ALTER TABLE `test_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
