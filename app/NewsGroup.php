<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsGroup extends Model
{
    protected $table = "news_group";

    public function news(){
        return $this->hasMany("App\News",'news_group_id','id');
    }
}
