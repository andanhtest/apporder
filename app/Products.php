<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table ="products";

    protected $fillable = [
        'name',
        'customer_type_id',
        'alias',
        'summary',
        'content',
        'images',
        'price',
        'price_old',
        'category_id'
    ];

    public function category_ref(){
        return $this->belongsTo('App\Categorys','category_id','id');
    }
}
