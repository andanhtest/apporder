<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorys extends Model
{
    protected $table ="categorys";

    public function product_ref(){
        return $this->hasMany("App\Products",'category_id','id');
    }
}
