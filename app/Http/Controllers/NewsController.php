<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\NewsGroup;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    //
    public function index(){

    }
    public function listNews(){
        $lists = News::orderBy('id','desc')->paginate(15);
        return view("admin.news.list",['lists'=>$lists]);
    }
    public function addNews(){
        $lists = NewsGroup::orderBy('id','asc')->get();   
        return view('admin.news.add',['list_news_group'=>$lists]);
    }
    public function editNews(Request $res){
        $news = News::where("id","=",$res->id)->first();
        $lists = NewsGroup::orderBy('id','asc')->get();   
        return view("admin.news.edit",['data'=>$news,'list_news_group'=>$lists]);
    }
    //post
    public function addNewsAction(Request $res){
        $newsgroup = new News();
        $newsgroup->name = $res->title;
        $newsgroup->alias = Helper::convert_alias($res->title);
        $newsgroup->summary = $res->summary;
        $newsgroup->content = Helper::changeUrlDomain($res->content);
        $newsgroup->news_group_id = $res->news_group_id;
        $newsgroup->status = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/newsgroup/add")->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists(env('APP_URL_UPLOAD')."/uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
        }else{
            $image = "";
        }
        $newsgroup->images = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        $newsgroup->save();
        return redirect("admin/news/list");
    }
    public function editNewsAction(Request $res){
        $newsgroup = News::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->title;
        $data['summary'] = $res->summary;
        $data['content'] = Helper::changeUrlDomain($res->content);
        $data['status'] = $res->status==1?1:0;
        $data['news_group_id'] = $res->news_group_id;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/news/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists(env('APP_URL_UPLOAD')."/uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['images'] = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        }
        $newsgroup->update($data);
        return redirect("admin/news/list");
    }
	public function deleteNewsAction(Request $res){        
        News::where("id","=",$res->id)->delete();
        return redirect("admin/news/list");
    }
}
