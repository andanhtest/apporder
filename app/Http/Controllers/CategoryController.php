<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorys;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function list(){
        $lists = Categorys::orderBy('id','desc')->get();
        return view("admin.category.list",['lists'=>$lists]);
    }
    public function add(){
        return view('admin.category.add');
    }
    public function edit(Request $res){
        $data = Categorys::where("id","=",$res->id)->first();
        return view("admin.category.edit",['data'=>$data]);
    }

    public function editCategoryAction(Request $res){        
        $cate = Categorys::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->title;
        $data['summary'] = $res->summary;
        $data['content'] = $res->content;
        $data['status'] = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/category/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
            //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['images'] = $image;
        }
        $cate->update($data);
        return redirect("admin/category/list");
    }
    //action
    public function addCategoryAction(Request $res){    
        $data = new Categorys();
        $data->name = $res->title;
        $data->alias = Helper::convert_alias($res->title);
        $data->summary = $res->summary;
        $data->content = $res->content;
        $data->status = $res->status==1?1:0;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/category/add")->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
            $file->move("uploads/images",$image);
           // Storage::disk('sftp')->put($image, fopen($file, 'r+'));
        }else{
            $image = "";
        }
        $data->images = $image;
        $data->save();
        return redirect("admin/category/list");
    }    
    public function deleteCategoryAction(Request $res){        
        Categorys::where("id","=",$res->id)->delete();
        return redirect("admin/category/list");
    }
}
