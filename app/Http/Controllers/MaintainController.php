<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetail;
use App\Helpers\Helper;
use App\Helpers\OnePay;

class MaintainController extends Controller
{
    public function listOrderStatusMaintain(){
        $lists = array();
        $data_current = date('Y-m-d H:i:s');
        $lists = Orders::where("date_order_payment_limit",'<',$data_current)
                        ->where('status',0)
                        ->where('type',2)
                        ->orderBy('id','desc');

        if($lists->count()>0)
        {
            $data['status'] = -1;    
            $lists->update($data);
        }

        echo "done";

    } 
}
