<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UrlWebsite;

class UrlWebsiteController extends Controller
{
    public function index(){
        echo "index";
        exit;
    }
    public function listUrl(){   
        $lists = UrlWebsite::orderBy('id','desc')->get();     
        return view("admin.url_website.list",['lists'=>$lists]);
    }
    public function addUrl(){        
        return view("admin.url_website.add");
    }
    public function editUrl(Request $res){   
        $data = UrlWebsite::where("id","=",$res->id)->first();
        return view("admin.url_website.edit",['data'=>$data]);
    }
    public function editUrlAction(Request $res){        
        $Url = UrlWebsite::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->name;
        $data['summary'] = $res->summary;
        $data['url'] = $res->url;
        $Url->update($data);
        return redirect("admin/url_website/list");
    }
    //action
    public function addUrlAction(Request $res){    
        $Url = new UrlWebsite();
        $Url->name = $res->name;
        $Url->summary = $res->summary;
        $Url->url = $res->url;
        $Url->save();
        return redirect("admin/url_website/list");
    }    
    public function deleteUrlAction(Request $res){        
        UrlWebsite::where("id","=",$res->id)->delete();
        return redirect("admin/url_website/list");
    }
}
