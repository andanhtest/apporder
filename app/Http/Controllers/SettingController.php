<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Setting;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    
    public function edit(Request $res){

        $data = Setting::first();
        return view("admin.setting.edit",['data'=>$data]);
    }

    public function editSettingAction(Request $res){        
        $cate = Setting::where("id","=",$res->id);
        $data = array();
        $data['company'] = $res->company;
        $data['summary'] = $res->summary;
        $data['url_website'] = $res->url_website;
        $data['money_format'] = $res->money_format;
        $data['timezone'] = $res->timezone;
        $data['copyright'] = $res->copyright;
        if($res->hasFile("image")){
            $file = $res->file("image");
            $ext = $file->getClientOriginalExtension();
            if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext !='gif'){
                return redirect("admin/setting/edit/".$res->id)->with('Error','only upload image type jpg, png jpeg or gif');
            }
            $name= $file->getClientOriginalName();
            $image = str_random(8)."_".$name;
            while(file_exists("uploads/images/".$image)){
                $image = str_randome(8)."_".$name;
            }
           $file->move("uploads/images",$image);
           //Storage::disk('sftp')->put($image, fopen($file, 'r+'));
            $data['logo'] = env('APP_URL_UPLOAD')."/uploads/images/".$image;
        }
        $cate->update($data);
        return redirect("admin/setting");
    }
}
