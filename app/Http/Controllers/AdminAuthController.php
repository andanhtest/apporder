<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\admins;
use App\Helpers\Helper;
use Session;

class AdminAuthController extends Controller
{
    function checkTokenExpiry(){
        if(Auth::guard('admin')->check()){
            view()->share('admin_login',Auth::guard('admin')->user());           
        }
    }
    public function index(){    
       Helper::checkExistLogin();
         return view("admin.auth.login");
     }
    public function login_admin(Request $res){        
       $u = $res['username'];
       $p = $res['password'];
       if(Auth::guard('admin')->attempt(['email'=>$u,'password'=>$p])){
            $admin = Admins::where('email', '=', $u)->first();

            $admin->token_expiry = Helper::generate_token();
            $admin->status = 1;
            $admin->save();
            Session::put("admin",$admin);
           return redirect('admin/');
       }else{
        return redirect('admin/login');
       }
    }
    public function logout(){
        Auth::guard('admin')->logout();
        Session::forget("admin");
        return redirect('admin/login');
    }
    public function checkLogin(){
        if(Auth::guard('admin')->check()){
            echo "da login";
            $admin = Auth::guard('admin')->user();
            echo "<pre>";
            if($admin->token_expiry < time()){
                echo "timeout";
                //Auth::guard('admin')->logout();
            }else{
                echo "timein";
            }
            echo "<pre>";
            print_r($admin);
        }else{
            echo "chua login";
        }
    }
    public function checkHelper(){
        echo Helper::generate_token();
    }
    public function getSession(){
        Session::put("abc","afdhasgda");
        Session::get("abc");
        if(Auth::guard('admin')->check()){
            echo "da login";
            $admin = Auth::guard('admin')->user();
            Session::put("admin",$admin);
            echo "<pre>";
            if($admin->token_expiry < time()){
                echo "timeout";
                //Auth::guard('admin')->logout();
            }else{
                echo "timein";
            }
            echo "<pre>";
            //print_r($admin);
        }else{
            echo "chua login";
        }
        if(Session::has('admin')){
            echo "co";
        }else{
            echo "ko";
        }
        //print_r(Session::get("admin"));
    }
}
