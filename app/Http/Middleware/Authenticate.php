<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            header("Content-Type: application/json; charset=UTF-8");
            //return route('login');
            //$response = array('success' => false , 'error_messages' => "token expriced" , 'error_code' => 133);
            //return response()->json($response, 200);
            $ms = array("message"=>"token expired","error_code"=>1000);
            echo json_encode($ms); exit;
        }
    }
}
