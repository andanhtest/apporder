<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\Helpers\Helper;

	class OnePay {
    /***
	------
	*/
	public static function paymentOnepay($dataPost){
		$appendAmp = 0;
		$vpcURL = "";
		$stringHashData = "";
		$data['FullName']	= $dataPost['name'];
		$data['City']		= $dataPost['city'];
		$data['District']	= $dataPost['district'];
		$data['Ward']		= $dataPost['ward'];
		$data['Phone']		= $dataPost['phone'];
		$data['Email']		= $dataPost['email'];
		$data['Address']		= $dataPost['address'];
		$data['UserID']			= $dataPost["user_id"];
		$data['Total']			= $dataPost['total'];
		$data['IPAddress']		= $_SERVER['REMOTE_ADDR'];			
		$data['OrderType']		= $dataPost['hinhthuctt'];
		$data['OrderID']		= $dataPost['order_id'];
		
		//vpc_Amount Số tiền cần thanh toán, nhân với 100. VD: 100=1VND
		//$Link = "https://mtf.onepay.vn/onecomm-pay/vpc.op?";
		$Link = "https://onepay.vn/onecomm-pay/vpc.op?";				

		//$Link_national = "https://mtf.onepay.vn/vpcpay/vpcpay.op?";
		$Link_national = "https://onepay.vn/vpcpay/vpcpay.op?";								
		
		$str_vpc_Customer_Id 	= str_replace("-"," ",Helper::convert_alias(str_replace("-"," ",($data['FullName']))));
		$str_vpc_SHIP_Country 	= str_replace("-"," ",($data['City']));
		$str_vpc_SHIP_Provice 	= str_replace("-"," ",($data['District']));
		$str_vpc_SHIP_Street01 	= str_replace("-"," ",($data['Address']));
		$str_vpc_SHIP_Street01 	= Helper::convert_alias($str_vpc_SHIP_Street01);
		
		
		//noi dia					
		if($data['OrderType']==1){
			// VPC_ACCESSCODE_BANK =D67342C2
			// VPC_MERCHANT_BANK =ONEPAY
			$SECURE_SECRET = env("SECURE_SECRET_PAYMENT_BANK");
			$data2['Title']					= $data['OrderID'];
			$data2['vpc_AccessCode'] 		= env("VPC_ACCESSCODE_BANK");//"D67342C2";
			$data2['vpc_Amount'] 			= 100*($data['Total']);
			$data2['vpc_Command'] 			= "pay";
			$data2['vpc_Currency'] 			= "VND";					
			$data2['vpc_Customer_Email'] 	= $data['Email'];
			$data2['vpc_Customer_Id'] 		= $str_vpc_Customer_Id;
			$data2['vpc_Customer_Phone'] 	= $data['Phone'];				
			$data2['vpc_Locale']			= "vn";
			$data2['vpc_MerchTxnRef'] 		= md5(date("YmdHis"));
			$data2['vpc_Merchant'] 			= env("VPC_MERCHANT_BANK");//"ONEPAY";
			$data2['vpc_OrderInfo'] 		= $data['OrderID'];
			$data2['vpc_ReturnURL'] 		= "http://".$_SERVER['SERVER_NAME']."/payment/complete";
			$data2['vpc_SHIP_City'] 		= "VietNam";
			$data2['vpc_SHIP_Country'] 		= $str_vpc_SHIP_Country;
			$data2['vpc_SHIP_Provice'] 		= $str_vpc_SHIP_Provice;
			$data2['vpc_SHIP_Street01'] 	= $str_vpc_SHIP_Street01;
			$data2['vpc_TicketNo'] 			= $_SERVER['REMOTE_ADDR'];
			$data2['vpc_Version'] 			= "2";				
			foreach($data2 as $key=>$value){
					if (strlen($value) > 0) {
					if ($appendAmp == 0) {
						$vpcURL .= urlencode($key) . '=' . urlencode($value);
						$appendAmp = 1;
					} else {
						$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
					}
					if ((strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
						$stringHashData .= $key . "=" . $value . "&";
					}
				}						
			}					
			$stringHashData = rtrim($stringHashData, "&");
			if (strlen($SECURE_SECRET) > 0) {
				$vpcURL .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*',$SECURE_SECRET)));
			}
			//echo $Link.$vpcURL; exit;			
			//log order payment
			//curl_exec($Link.$vpcURL);
			header('Location: '.$Link.$vpcURL);
			exit;
			return true;	
		}
		else if($data['OrderType']==2){		
			// VPC_ACCESSCODE_VISA =6BEB2546
			// VPC_MERCHANT_VISA =TESTONEPAY			
			//quoc te
			$SECURE_SECRET = env("SECURE_SECRET_PAYMENT_VISA");										
			$data2 =array();
			$data2['AVS_City']				= "";
			$data2['AVS_Country']			= "";
			$data2['AVS_PostCode']			= "";
			$data2['AVS_StateProv']			= "";
			$data2['AVS_Street01']			= "";
			$data2['AgainLink']				= "http://".$_SERVER['SERVER_NAME']."/go-home";;//urlencode($_SERVER['HTTP_REFERER']);						
			$data2['Title']					= "DH-".$data['OrderID'];
			$data2['display'] 				= "";
			$data2['vpc_AccessCode'] 		= env("VPC_ACCESSCODE_VISA");//"6BEB2546";
			$data2['vpc_Amount'] 			= 100*($data['Total']);
			$data2['vpc_Command'] 			= "pay";					
			$data2['vpc_Customer_Email'] 	= $data['Email'];
			$data2['vpc_Customer_Id'] 		= $str_vpc_Customer_Id;
			$data2['vpc_Customer_Phone'] 	= $data['Phone'];				
			$data2['vpc_Locale']			= "en";
			$data2['vpc_MerchTxnRef'] 		= md5(date("YmdHis"));
			$data2['vpc_Merchant'] 			= env("VPC_MERCHANT_VISA");//"TESTONEPAY";
			$data2['vpc_OrderInfo'] 		= $data['OrderID'];
			$data2['vpc_ReturnURL'] 		= "http://".$_SERVER['SERVER_NAME']."/payment/complete_visa";
			$data2['vpc_SHIP_City'] 		= "VietNam";//$data['City'];
			$data2['vpc_SHIP_Country'] 		= $str_vpc_SHIP_Country;
			$data2['vpc_SHIP_Provice'] 		= $str_vpc_SHIP_Provice;
			$data2['vpc_SHIP_Street01'] 	= $str_vpc_SHIP_Street01;//$data['Address'];
			
			$data2['vpc_TicketNo'] 			= $_SERVER['REMOTE_ADDR'];	
			$data2['vpc_Version'] 			= "2";
									
			foreach($data2 as $key=>$value){
					if (strlen($value) > 0) {
					if ($appendAmp == 0) {
						$vpcURL .= urlencode($key) . '=' . urlencode($value);
						$appendAmp = 1;
					} else {
						$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
					}
					if ((strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
						$stringHashData .= $key . "=" . $value . "&";
					}
				}						
			}					
			$stringHashData = rtrim($stringHashData, "&");
			if (strlen($SECURE_SECRET) > 0) {
				$vpcURL .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*',$SECURE_SECRET)));
			}		
			//curl_exec($Link_national.$vpcURL);
			//echo $Link_national.$vpcURL; exit;
			header('Location: '.$Link_national.$vpcURL);exit;
			return true;
		}
		else{					
			return true;
		}							
			
	}
	public static function null2unknown($data) {
		if ($data == "") {
			return "No Value Returned";
		} else {
			return $data;
		}
	}
	public static function getResponseDescription($responseCode) {
	
		switch ($responseCode) {
			case "0" :
				$result = "Giao dịch thành công - Approved";
				break;
			case "1" :
				$result = "Ngân hàng từ chối giao dịch - Bank Declined";
				break;
			case "3" :
				$result = "Mã đơn vị không tồn tại - Merchant not exist";
				break;
			case "4" :
				$result = "Không đúng access code - Invalid access code";
				break;
			case "5" :
				$result = "Số tiền không hợp lệ - Invalid amount";
				break;
			case "6" :
				$result = "Mã tiền tệ không tồn tại - Invalid currency code";
				break;
			case "7" :
				$result = "Lỗi không xác định - Unspecified Failure ";
				break;
			case "8" :
				$result = "Số thẻ không đúng - Invalid card Number";
				break;
			case "9" :
				$result = "Tên chủ thẻ không đúng - Invalid card name";
				break;
			case "10" :
				$result = "Thẻ hết hạn/Thẻ bị khóa - Expired Card";
				break;
			case "11" :
				$result = "Thẻ chưa đăng ký sử dụng dịch vụ - Card Not Registed Service(internet banking)";
				break;
			case "12" :
				$result = "Ngày phát hành/Hết hạn không đúng - Invalid card date";
				break;
			case "13" :
				$result = "Vượt quá hạn mức thanh toán - Exist Amount";
				break;
			case "21" :
				$result = "Số tiền không đủ để thanh toán - Insufficient fund";
				break;
			case "99" :
				$result = "Người sủ dụng hủy giao dịch - User cancel";
				break;
			default :
				$result = "Giao dịch thất bại - Failured";
		}
		return $result;
	}
}