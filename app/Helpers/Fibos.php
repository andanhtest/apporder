<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use Hash;

class Fibos {
    
    private $_fiboAccount;
    private $_securityPassword;
    private static $_fiboServerBaseUrl;
    private $_msgPrefix;

    function __construct() {
        // define("CONFIG_ACCOUNT", "CL1610240004");
        // define("CONFIG_SECURITY_PASSWORD", "jT4tRHuub89gQytQ");
        // define("CONFIG_SERVER_BASE_URL", "http://center.fibosms.com/service.asmx/");
        // define("CONFIG_PREFIX", ""); //using with FiboSMSHosting
        $this->_securityPassword 		= "jT4tRHuub89gQytQ";
        $this->_fiboServerBaseUrl 	= "http://center.fibosms.com/service.asmx/";
        $this->_fiboAccount = "CL1610240004";
        $this->_msgPrefix = "";
    }
    public static function xmlstr_to_array($xmlstr) {
        $doc = new DOMDocument();
        $doc->loadXML($xmlstr);
        return domnode_to_array($doc->documentElement);
    }
    private static function _processXmlResponse($url){
		$rs = "";
        if (extension_loaded('curl')) {
			//echo $url;
            $ch = curl_init() or die ( curl_error($ch) );
            $timeout = 10;
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec( $ch );
            curl_close( $ch );
            //header('Content-type: application/json');
            $xml = simplexml_load_string($data);
            $json = json_encode($xml);
            //$str = "Hello world. It's a beautiful day.";
            $fet1 = (explode("<Code>",$json));
            $fet2 = (explode("<\/Code>",$fet1[1]));
            return ($fet2[0]); 
			/*exit;
            $array = json_decode($json,TRUE);
            exit;
            if($data){
                return self::xmlstr_to_array(new SimpleXMLElement($data));
            }
            else
                return false;*/
        }
        return true;//$this->xmlstr_to_array(simplexml_load_file($url));
    }
    
    public static function getClientBalance($createdParams){
        /**
         * http://center.fibosms.com/Service.asmx/GetClientBalance?clientNo=string&clientPass=string&serviceType=string
         */
        $_securityPassword 		= "jT4tRHuub89gQytQ";
        $_fiboServerBaseUrl 	= "http://center.fibosms.com/service.asmx/";
        $_fiboAccount = "CL1610240004";
        $url = $_fiboServerBaseUrl."GetClientBalance?";
        $params = "clientNo=".urlencode($_fiboAccount)."&clientPass=".$_securityPassword.'&serviceType='.$createdParams['serviceType'];
        
       // echo $url.$params;

       return $xml = self::_processXmlResponse($url.$params);
       /* if($xml) {
            //echo "ok";exit;
            if(isset($xml['Code']))
                return array(
                    'returnCode' => $xml['Code'],
                    'message' => $xml['AccountBalance'],
                    'time' => $xml['Time']
                );
            return array(
                'returnCode' => null,
                'message' => $xml['Message'],
                'time' => null
            );
        }
        else {
            return null;
        }*/
    }
    
    public static function sendMaskedSms($createdParams){
        //http://center.fibosms.com/service.asmx/SendMaskedSMS?clientNo=string&clientPass=string&senderName=&phoneNumber=string&smsMessage=string&smsGUID=string&serviceType=string
		$createdParams['smsMessage'] = $createdParams['smsMessage'];
        $_securityPassword 		= "jT4tRHuub89gQytQ";
        $_fiboServerBaseUrl 	= "http://center.fibosms.com/service.asmx/";
        $_fiboAccount = "CL1610240004";
        $url = $_fiboServerBaseUrl."SendMaskedSMS?";
        $params = "senderName=ThanhKimSon&clientNo=".urlencode($_fiboAccount)."&clientPass=".$_securityPassword."&phoneNumber=".$createdParams['phoneNumber']."&smsMessage=".urlencode(self::removeAccent($createdParams['smsMessage'])).'&smsGUID='.$createdParams['smsGUID'].'&serviceType='.$createdParams['serviceType'];
		//echo $url.$params; exit;
        return $xml = self::_processXmlResponse($url.$params);
        if($xml) {
            if(isset($xml['Code']))
                return array(
                    'returnCode' => $xml['Code'],
                    'message' => $xml['Message'],
                    'time' => $xml['Time']
                );
            return array(
                'returnCode' => null,
                'message' => $xml['Message'],
                'time' => null
            );
        }
        else {
            return null;
        }

    }    
    /**
     * FStar Team
     * @author Klaus
     * July 14th 2013
     */
    public static function removeAccent( $str )
    {
        $trans = array(
            "đ" => "d", "ă" => "a", "â" => "a", "á" => "a", "à" => "a", "ả" => "a", "ã" => "a", "ạ" => "a",
            "ấ" => "a", "ầ" => "a", "ẩ" => "a", "ẫ" => "a", "ậ" => "a", "ắ" => "a", "ằ" => "a", "ẳ" => "a",
            "ẵ" => "a", "ặ" => "a", "é" => "e", "è" => "e", "ẻ" => "e", "ẽ" => "e", "ẹ" => "e", "ế" => "e",
            "ề" => "e", "ể" => "e", "ễ" => "e", "ệ" => "e", "í" => "i", "ì" => "i", "ỉ" => "i", "ĩ" => "i",
            "ị" => "i", "ư" => "u", "ô" => "o", "ơ" => "o", "ê" => "e", "Ư" => "u", "Ô" => "o", "Ơ" => "o",
            "Ê" => "e", "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u", "ứ" => "u", "ừ" => "u",
            "ử" => "u", "ữ" => "u", "ự" => "u", "ó" => "o", "ò" => "o", "ỏ" => "o", "õ" => "o", "ọ" => "o",
            "ớ" => "o", "ờ" => "o", "ở" => "o", "ỡ" => "o", "ợ" => "o", "ố" => "o", "ồ" => "o", "ổ" => "o",
            "ỗ" => "o", "ộ" => "o", "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u", "ứ" => "u",
            "ừ" => "u", "ử" => "u", "ữ" => "u", "ự" => "u", 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y',
            'ỵ' => 'y', 'Ý' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', "Đ" => "D", "Ă" => "A",
            "Â" => "A", "Á" => "A", "À" => "A", "Ả" => "A", "Ã" => "A", "Ạ" => "A", "Ấ" => "A", "Ầ" => "A",
            "Ẩ" => "A", "Ẫ" => "A", "Ậ" => "A", "Ắ" => "A", "Ằ" => "A", "Ẳ" => "A", "Ẵ" => "A", "Ặ" => "A",
            "É" => "E", "È" => "E", "Ẻ" => "E", "Ẽ" => "E", "Ẹ" => "E", "Ế" => "E", "Ề" => "E", "Ể" => "E",
            "Ễ" => "E", "Ệ" => "E", "Í" => "I", "Ì" => "I", "Ỉ" => "I", "Ĩ" => "I", "Ị" => "I", "Ư" => "U",
            "Ô" => "O", "Ơ" => "O", "Ê" => "E", "Ư" => "U", "Ô" => "O", "Ơ" => "O", "Ê" => "E", "Ú" => "U",
            "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U", "Ứ" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U",
            "Ự" => "U", "Ó" => "O", "Ò" => "O", "Ỏ" => "O", "Õ" => "O", "Ọ" => "O", "Ớ" => "O", "Ờ" => "O",
            "Ở" => "O", "Ỡ" => "O", "Ợ" => "O", "Ố" => "O", "Ồ" => "O", "Ổ" => "O", "Ỗ" => "O", "Ộ" => "O",
            "Ú" => "U", "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U", "Ứ" => "U", "Ừ" => "U", "Ử" => "U",
            "Ữ" => "U", "Ự" => "U", "'"=> "-", '"' => '-'
        );

        return strtr($str, $trans);

    }
	public static function generateRandomCode($characters=5) 
	{
	   $possible = '0123456789';
	   $code = '';
	   $i = 0;
	   while ($i < $characters) {
		   $code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
		   $i++;
	   }
	   return $code;
	}

}
