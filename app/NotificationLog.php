<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationLog extends Model
{
    protected $table = "notification_log";


    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
