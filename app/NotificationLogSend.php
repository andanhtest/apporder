<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationLogSend extends Model
{
    protected $table = "_notification_send";

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
