<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "notification";

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
