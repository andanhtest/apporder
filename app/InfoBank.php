<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoBank extends Model
{
    protected $table = "info_bank";
	
	protected $hidden = ["created_at", "updated_at"];
}
