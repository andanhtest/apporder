<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";

    public function news_group(){
        return $this->belongsTo('App\NewsGroup','news_group_id','id');
    }
}
