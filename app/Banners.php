<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table ="banners";

    public function banner_group(){
        return $this->belongsTo('App\BannerGroup','type','id');
    }
}
