@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Danh sách
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Danh sách</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh sách <!--a href="admin/notification/add">[thêm mới]</a--></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Stt</th>
                  <th>Tiêu đề</th>
                  <th>Nội dung</th>
                  <th>Ngày</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lists as $k=>$v)
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$v->title}}</td>
                  <td>{{$v->summary}}</td>
                  <td>{{$v->created_at}}</td>
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            @if(count($lists)>0)
              {{ $lists->links() }}
            @endif
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection