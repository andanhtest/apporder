@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($data))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              @if(isset($message))
              {{$message}}
              @endif
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('editSetting')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">                
                
                <div class="form-group">
                  <label for="title">Công ty</label>
                  <input required type="text" name="company" value="{{$data->company}}" class="form-control" id="company" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Url</label>
                  <input required type="text" name="url_website" value="{{$data->url_website}}" class="form-control" id="url_website" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Đơn vị tiền tệ</label>
                  <input required type="text" name="money_format" value="{{$data->money_format}}" class="form-control" id="money_format" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Timezone</label>
                  <input required type="text" name="timezone" value="{{$data->timezone}}" class="form-control" id="timezone" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Copyright</label>
                  <input required type="text" name="copyright" value="{{$data->copyright}}" class="form-control" id="copyright" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="summary">Mô tả</label>
                  <textarea name="summary" class="form-control" id="" placeholder="Mô tả">{{$data->summary}}</textarea>
                </div>
                <div class="form-group">
                  @if($data->logo !="")
                  <img src="{{$data->logo}}" style="width:100px; max-height:200px"/>
                  @endif
                  <br>
                  <label for="exampleInputFile">Cập nhật hình</label>
                  <input type="file" id="image" name="image">
                  <p class="help-block">chỉ dùng [.png .jpg .gif]</p>
                </div>

               
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
