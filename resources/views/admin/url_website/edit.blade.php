@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($data))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              @if(isset($message))
              {{$message}}
              @endif
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('editUrl')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Tiêu đề</label>
                  <input required type="text" name="name" value="{{$data->name}}" class="form-control" id="name" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Url</label>
                  <input required type="text" name="url" value="{{$data->url}}" class="form-control" id="url" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="summary">Mô tả</label>
                  <textarea name="summary" class="form-control" id="" placeholder="Mô tả">{{$data->summary}}</textarea>
                </div>

                

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
